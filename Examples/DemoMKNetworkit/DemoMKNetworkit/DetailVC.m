//
//  DetailVC.m
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailVC.h"
#define MyAppDelegate ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface DetailVC ()
@end

@implementation DetailVC
@synthesize movieEntry = _movieEntry;
@synthesize imgView = _imgView;
@synthesize txtView = _txtView;


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.movieEntry != nil) {
        NSURL *imageURL = [NSURL URLWithString:[[[self.movieEntry getElementsByTagName:@"im:image"] objectAtIndex:2] nodeValue]];
        NSLog(@"%@",imageURL.absoluteString);
        [MyAppDelegate.engine imageAtURL:imageURL onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
            self.imgView.image = fetchedImage;
        } ];
    }
}

- (void)viewDidUnload
{
    [self setImgView:nil];
    [self setTxtView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
