//
//  AppStoreEngine.m
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define APPLE_STORE_HOST @"itunes.apple.com"

#import "AppStoreEngine.h"
@interface AppStoreEngine()
@property (strong, nonatomic) DatoXMLParser* xmlParser;
@property (copy, nonatomic) TopMoviesResponseBlock completionBlock;
@property (copy, nonatomic) MKNKErrorBlock errorBlock;
@end

@implementation AppStoreEngine
@synthesize xmlParser = _xmlParser;
@synthesize completionBlock = _completionBlock;
@synthesize errorBlock = _errorBlock;

- (id)init{
    self = [super initWithHostName:APPLE_STORE_HOST];
    if (self) {
        [self useCache];
    }
    
    return self;
}

-(void)downloadTopMoviesListOnCompletion:(TopMoviesResponseBlock) completionBlock
                                 onError:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"us/rss/topmovies/limit=50/xml"
                                              params:nil
                                          httpMethod:@"GET"];
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    [op onCompletion:^(MKNetworkOperation *completedOperation)
     {
         [self parsingResponseData:completedOperation.responseData];
     }onError:^(NSError* error) {         
         errorBlock(error);
     }];
    
    [self enqueueOperation:op];
}

-(void)parsingResponseData:(NSData *)responseData{
    self.xmlParser = [[DatoXMLParser alloc] initWithData:responseData delegate:self];
    [self.xmlParser start];
}

#pragma mark - DatoXMLParserDelegate
-(void)datoXMLParserDidFinish:(id)sender xmlDocument:(DatoXMLNode *)xmlDOM{
    NSMutableArray *entryArray = [[xmlDOM getElementsByTagName:@"entry"] mutableCopy];
    self.completionBlock(entryArray);
}
@end
