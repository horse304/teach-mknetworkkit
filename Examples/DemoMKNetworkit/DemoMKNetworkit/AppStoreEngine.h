//
//  AppStoreEngine.h
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MKNetworkEngine.h"
#import "DatoXMLParser.h"

typedef void (^TopMoviesResponseBlock)(NSMutableArray * topMovies);

@interface AppStoreEngine : MKNetworkEngine <DatoXMLParserDelegate>

-(void)downloadTopMoviesListOnCompletion:(TopMoviesResponseBlock) completionBlock
                                 onError:(MKNKErrorBlock) errorBlock;

@end
