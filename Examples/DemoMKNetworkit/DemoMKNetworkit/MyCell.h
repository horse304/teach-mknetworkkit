//
//  MyCell.h
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface MyCell : UITableViewCell
-(void)setThumbnail:(NSURL *)imageURL;
@end
