//
//  ViewController.m
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

#define MyAppDelegate ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface ViewController ()
@property (retain, nonatomic) NSMutableArray *topMovies;
@property (retain, nonatomic) DetailVC *detailVC;
@end

@implementation ViewController
@synthesize tableView;
@synthesize topMovies = _topMovies;
@synthesize detailVC = _detailVC;

-(void)loadView{
    [super loadView];
    
    //Add empty cache button
    UIBarButtonItem * emptyCacheBtn = [[UIBarButtonItem alloc] initWithTitle:@"Empty Cache" style:UIBarButtonItemStylePlain target:self action:@selector(touchOnEmptyCache:)];
    self.navigationItem.leftBarButtonItem = emptyCacheBtn;
    
    //Add title
    self.navigationItem.title = @"Top Movies";
}

-(void)viewDidLoad{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [MyAppDelegate.engine downloadTopMoviesListOnCompletion:^(NSMutableArray *topMovies) {
        self.topMovies = topMovies;
        [self.tableView reloadData];
    } onError:^(NSError *error) {
        NSLog(@"error %@",error);
    } ];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.topMovies == nil) {
        return 0;
    }else {
        return self.topMovies.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)mytableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.topMovies != nil) {
        MyCell *cell = (MyCell*)[mytableView dequeueReusableCellWithIdentifier:@"mycell"];
        if (cell == nil) {
            cell = [[MyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mycell"];
        }
        DatoXMLNode *entry = [self.topMovies objectAtIndex:indexPath.row];
        DatoXMLNode *title = [[entry getElementsByTagName:@"title"] objectAtIndex:0];
        NSURL *imageURL = [NSURL URLWithString:[[[entry getElementsByTagName:@"im:image"] objectAtIndex:2] nodeValue]];
        [cell setThumbnail:imageURL];
        cell.textLabel.text = title.nodeValue;
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.detailVC == nil) {
        self.detailVC = [[DetailVC alloc] initWithNibName:@"DetailVC" bundle:nil];
    }
    self.detailVC.movieEntry = [self.topMovies objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.detailVC animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)touchOnEmptyCache:(id)sender{
    [MyAppDelegate.engine emptyCache];
}

@end
