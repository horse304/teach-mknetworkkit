//
//  AppDelegate.h
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppStoreEngine.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AppStoreEngine *engine;

@property (strong, nonatomic) ViewController *viewController;

@end
