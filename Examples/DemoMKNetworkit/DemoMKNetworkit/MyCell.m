//
//  MyCell.m
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyCell.h"
#define MyAppDelegate ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface MyCell()
@property (nonatomic, retain) MKNetworkOperation *imageLoadingOperation;
@end

@implementation MyCell
@synthesize imageLoadingOperation = _imageLoadingOperation;

-(void)prepareForReuse{
    self.imageView.image = nil;
    [self.imageLoadingOperation cancel];
}


-(void)setThumbnail:(NSURL *)imageURL{
    self.imageLoadingOperation = [MyAppDelegate.engine imageAtURL:imageURL onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
        
        self.imageView.image = fetchedImage;
        [self setNeedsLayout];
    }];
}

@end
