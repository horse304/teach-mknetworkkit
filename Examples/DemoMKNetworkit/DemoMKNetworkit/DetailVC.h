//
//  DetailVC.h
//  DemoMKNetworkit
//
//  Created by Techmaster on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatoXMLNode.h"
#import "AppDelegate.h"

@interface DetailVC : UIViewController

@property (retain, nonatomic) DatoXMLNode *movieEntry;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
@end
