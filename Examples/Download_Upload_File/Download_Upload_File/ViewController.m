//
//  ViewController.m
//  Download_Upload_File
//
//  Created by Dato on 10/26/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//
#import "AppDelegate.h"
#define APPDELEGATE_MYENGINE ((AppDelegate *)[UIApplication sharedApplication].delegate).myEngine
#define DOWNLOAD_URL_STRING @"https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/StoreKitGuide.pdf"
//@"https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/Streams/Streams.pdf"
#define DOCUMENT_DIRECTORY_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, retain) MKNetworkOperation * downloadOperation;
@property (nonatomic, retain) MKNetworkOperation * uploadOperation;
@end

@implementation ViewController
@synthesize downloadOperation = _downloadOperation;
@synthesize uploadOperation = _uploadOperation;


- (IBAction)touchOnDownload:(id)sender {
    if (self.downloadOperation == nil ||
        self.downloadOperation.isFinished ||
        self.downloadOperation.isCancelled) {
        
        NSURL *fileURL = [NSURL URLWithString:DOWNLOAD_URL_STRING];
        NSString *destinationPath = [DOCUMENT_DIRECTORY_PATH stringByAppendingPathComponent:fileURL.lastPathComponent];
        self.downloadOperation = [APPDELEGATE_MYENGINE downloadFile:fileURL toPath:destinationPath OnCompletion:^(MKNetworkOperation *completedOperation) {
            
            //Thay doi text cua btnDownload tu Cancel -> Download
            [self.btnDownload setTitle:@"Download" forState:UIControlStateNormal];
            
            //Alert duong dan file da luu
            NSString *message = [NSString stringWithFormat:@"Saved to %@",destinationPath];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                             message:message
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil];
            [alert show];
        } onError:^(NSError *error) {
            //Thay doi text cua btnDownload tu Cancel -> Download
            [self.btnDownload setTitle:@"Download" forState:UIControlStateNormal];
            ELog(error);
        } onProgressChanged:^(double progress, long total_size) {
            //Thay doi text cua btnDownload tu Download -> Cancel
            [self.btnDownload setTitle:@"Cancel" forState:UIControlStateNormal];
            
            //Update progress view
            self.progressDownload.progress = progress;
            
            //Update label progress
            float total_size_mb = ((float)total_size)/(1024*1024);
            float downloaded_size_mb = progress * total_size_mb;
            self.lblProgressDownload.text = [[NSString alloc] initWithFormat:@"%.1f/%.1f Mb",downloaded_size_mb,total_size_mb];
        }];        
    }else{
        if ([self.downloadOperation isExecuting]) {
            [self.downloadOperation cancel];
            
            //Thay doi text cua btnDownload tu Cancel -> Resume
            [self.btnDownload setTitle:@"Resume" forState:UIControlStateNormal];
            
            self.downloadOperation = nil;
        }
    }
}

- (IBAction)touchOnUpload:(id)sender {
    if (self.uploadOperation == nil ||
        self.uploadOperation.isCancelled ||
        self.uploadOperation.isFinished) {
        
        NSString *fileUploadPath = [[NSBundle mainBundle] pathForResource:@"ipad_mini" ofType:@"jpg"];
        self.uploadOperation = [APPDELEGATE_MYENGINE uploadFile:fileUploadPath onCompletion:^(MKNetworkOperation *completedOperation) {
            NSLog(@"Response: %@", completedOperation.responseJSON);
            
            //Thay doi text cua btnUpload tu Cancel -> Upload
            [self.btnUpload setTitle:@"Upload" forState:UIControlStateNormal];
            
            //Alert duong dan file da luu
            NSString *message = [NSString stringWithFormat:@"Link to file %@", [completedOperation.responseJSON valueForKey:@"link"]];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                             message:message
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles: nil];
            [alert show];
        } onError:^(NSError *error) {            
            //Thay doi text cua btnUpload tu Cancel -> Upload
            [self.btnUpload setTitle:@"Upload" forState:UIControlStateNormal];
            ELog(error);
        } onProgressChanged:^(double progress, long total_size) {
            
            //Thay doi text cua btnUpload tu Upload -> Cancel
            [self.btnUpload setTitle:@"Cancel" forState:UIControlStateNormal];
            
            //Update progress view
            self.progressUpload.progress = progress;
            
            //Update label progress
            float total_size_mb = ((float)total_size)/(1024*1024);
            float uploaded_size_mb = progress * total_size_mb;
            self.lblProgressUpload.text = [[NSString alloc] initWithFormat:@"%.1f/%.1f Mb",uploaded_size_mb,total_size_mb];
        }];
    }else{
        if ([self.uploadOperation isExecuting]) {
            [self.uploadOperation cancel];
            self.uploadOperation = nil;
            //Thay doi text cua btnUpload tu Cancel -> Upload
            [self.btnUpload setTitle:@"Upload" forState:UIControlStateNormal];
        }
    }
}
@end
