//
//  AppDelegate.h
//  Download_Upload_File
//
//  Created by Dato on 10/26/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKNKMyEngine.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) MKNKMyEngine * myEngine;
@property (strong, nonatomic) UIWindow *window;

@end
