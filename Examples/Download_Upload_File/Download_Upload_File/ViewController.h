//
//  ViewController.h
//  Download_Upload_File
//
//  Created by Dato on 10/26/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnDownload;
@property (weak, nonatomic) IBOutlet UILabel *lblProgressDownload;
@property (weak, nonatomic) IBOutlet UIProgressView *progressDownload;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UILabel *lblProgressUpload;
@property (weak, nonatomic) IBOutlet UIProgressView *progressUpload;

- (IBAction)touchOnDownload:(id)sender;
- (IBAction)touchOnUpload:(id)sender;

@end
