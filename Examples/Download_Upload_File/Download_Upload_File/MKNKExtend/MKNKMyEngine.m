//
//  MKNKMyEngine.m
//  Download_Upload_File
//
//  Created by Dato on 10/26/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#define MY_HOST_NAME @"freeskyteam.com"
#define UPLOAD_PATH @"mknk_examples/uploader.php"
#import "MKNKMyEngine.h"

@implementation MKNKMyEngine

-(id)init{
    self = [super initWithHostName:MY_HOST_NAME];
    if (self) {
    }
    
    return self;
}

-(MKNetworkOperation *)downloadFile:(NSURL *)sourceURL
                             toPath:(NSString *)destinationPath
                       OnCompletion:(MKNKResponseBlock)completeBlock
                            onError:(MKNKErrorBlock)errorBlock
                  onProgressChanged:(MKNKMyEngineProgressBlock)progressBlock{
    
    __block MKNetworkOperation * op = [self operationWithURLString:sourceURL.absoluteString
                                                            params:nil
                                                        httpMethod:@"GET"];
    [op addDownloadStream:[[NSOutputStream alloc] initToFileAtPath:destinationPath
                                                            append:YES]];
    [op onDownloadProgressChanged:^(double progress) {
        long total_size = op.readonlyResponse.expectedContentLength;
        if (op.startPosition != 0) {
            //Neu resume thi total_size = startPosion + expectedContentLength
            total_size = total_size + op.startPosition;
        }
        progressBlock(progress, total_size);
    }];
    [op onCompletion:completeBlock onError:errorBlock];
    
    //Kiem tra file size downloaded, server neu ho tro se resume.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error;
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:destinationPath error:&error];
    if(error == NULL && [fileAttributes valueForKey:NSFileSize] != nil)
    {
        long downloaded_size = [[fileAttributes valueForKey:NSFileSize] longValue];
        
        //Set custom header for resume previous download
        NSMutableDictionary *customHeader = [[NSMutableDictionary alloc] init];
        [customHeader setValue:[NSString stringWithFormat:@"bytes=%ld-",downloaded_size]
                        forKey:@"Range"];
        [op addHeaders:customHeader];
    }
    
    //Add operation into shared queue of engine
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation *)uploadFile:(NSString *)filePath
                     onCompletion:(MKNKResponseBlock)completeBlock
                          onError:(MKNKErrorBlock)errorBlock
                onProgressChanged:(MKNKMyEngineProgressBlock)progressBlock{
    __block MKNetworkOperation * op = [self operationWithPath:UPLOAD_PATH
                                                       params:nil
                                                   httpMethod:@"POST"];
    //Attach file
    [op addFile:filePath forKey:@"file" mimeType:@"image/jpeg"];
    
    __block long sizeFileUpload;
    //Get size of filePath
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error;
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:&error];
    if(error == NULL && [fileAttributes valueForKey:NSFileSize] != nil)
    {
        sizeFileUpload = [[fileAttributes valueForKey:NSFileSize] longValue];
    }
    
    //Set progress handler
    [op onUploadProgressChanged:^(double progress) {        
        progressBlock(progress, sizeFileUpload );
    }];
    
    //Set completion and error handler
    [op onCompletion:completeBlock onError:errorBlock];
    
    //Add operation into shared queue of engine
    [self enqueueOperation:op];
    return op;
}
@end
