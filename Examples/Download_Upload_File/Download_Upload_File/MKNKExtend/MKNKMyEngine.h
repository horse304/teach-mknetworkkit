//
//  MKNKMyEngine.h
//  Download_Upload_File
//
//  Created by Dato on 10/26/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "MKNetworkEngine.h"

typedef void (^MKNKMyEngineProgressBlock)(double progress, long total_size);
@interface MKNKMyEngine : MKNetworkEngine

- (id)init;

-(MKNetworkOperation *)downloadFile:(NSURL *)sourceURL
                             toPath:(NSString *)destinationPath
                       OnCompletion:(MKNKResponseBlock)completeBlock
                            onError:(MKNKErrorBlock)errorBlock
                  onProgressChanged:(MKNKMyEngineProgressBlock)progressBlock;

-(MKNetworkOperation *)uploadFile:(NSString *)filePath
                     onCompletion:(MKNKResponseBlock)completeBlock
                          onError:(MKNKErrorBlock)errorBlock
                onProgressChanged:(MKNKMyEngineProgressBlock)progressBlock;
@end
