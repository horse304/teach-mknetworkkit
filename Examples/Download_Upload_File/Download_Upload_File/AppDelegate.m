//
//  AppDelegate.m
//  Download_Upload_File
//
//  Created by Dato on 10/26/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize myEngine = _myEngine;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _myEngine = [[MKNKMyEngine alloc] init];
    // Override point for customization after application launch.
    return YES;
}

@end
