<?php
$allowedExts = array("jpg", "jpeg", "gif", "png");
$extension = end(explode(".", $_FILES["file"]["name"]));
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/png")
|| ($_FILES["file"]["type"] == "image/pjpeg"))
&& ($_FILES["file"]["size"] < 2000000)
&& in_array($extension, $allowedExts))
{
    if ($_FILES["file"]["error"] > 0)
    {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
    else
    {
        $imageUploaded = array();
        $imageUploaded["isSuccess"] = TRUE;
        $imageUploaded["name"] = $_FILES["file"]["name"];
        $imageUploaded["type"] = $_FILES["file"]["type"];
        $imageUploaded["size"] = $_FILES["file"]["size"]; 
        $imageUploaded["isExisted"] = FALSE;
        $imageUploaded["link"] = "http://" . $_SERVER["SERVER_NAME"] . "/mknk_examples/upload/".$_FILES["file"]["name"];

        if (file_exists("upload/" . $_FILES["file"]["name"]))
        {
            $imageUploaded["isExisted"] = TRUE;
        }
        else
        {
            move_uploaded_file($_FILES["file"]["tmp_name"],"upload/" . $_FILES["file"]["name"]);
        }
        echo json_encode($imageUploaded);

    }
}
else
{
  echo "Invalid file";
}

?>
