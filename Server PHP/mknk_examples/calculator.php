<?php
    
if(isset($_GET["number1"]) && isset($_GET["number2"]) && isset($_GET["operation"])){	
	$number1 = $_GET["number1"];
	$number2 = $_GET["number2"];
	$operation = $_GET["operation"];
	
	$result = 0;
	switch ($operation) {
	    case "add":
	        $result = $number1 + $number2;
	
	        break;
	    case "sub":
	        $result = $number1 - $number2;
	
	        break;
	    case "multiply":
	        $result = $number1 * $number2;
	
	        break;
	    case "divide":
	        $result = $number1 / $number2;
	
	        break;
	
	    default:
	        break;
	}
	
	echo "Result is " . $result . " using GET";
}else if(isset($_POST["number1"]) && isset($_POST["number2"]) && isset($_POST["operation"])){
	$number1=0;
	if(isset($_POST["number1"])){
	    $number1 = $_POST["number1"];
	}
	
	$number2=0;
	if(isset($_POST["number2"])){
	    $number2 = $_POST["number2"];
	}
	
	$operation = "add";
	if(isset($_POST["operation"])){
	    $operation = $_POST["operation"];
	}
	
	$result = 0;
	switch ($operation) {
	    case "add":
	        $result = $number1 + $number2;
	
	        break;
	    case "sub":
	        $result = $number1 - $number2;
	
	        break;
	    case "multiply":
	        $result = $number1 * $number2;
	
	        break;
	    case "divide":
	        $result = $number1 / $number2;
	
	        break;
	
	    default:
	        break;
	}
	
	echo "Result is " . $result . " using POST";
}else{
	echo "Calculator by Dato - dat@techmaster.vn";
}
?>